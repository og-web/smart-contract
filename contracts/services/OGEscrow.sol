// SPDX-License-Identifier: MIT
pragma solidity 0.8.12;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/ERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/StringsUpgradeable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

import "../governance/ParameterControl.sol";
import "../goods/OGNFT.sol";

contract OGEscrow is
Initializable,
ReentrancyGuardUpgradeable,
AccessControlUpgradeable
{
    event MinterRefund(
        bytes32 redeemId,
        address minter,
        uint256 tokenId,
        address erc20,
        uint256 burnAmount,
        uint256 burnBalance
    );
    event MinterRedeem(
        bytes32 redeemId,
        address minter,
        uint256 tokenId,
        uint256 burnAmount,
        uint256 burnBalance
    );
    event UserRedeem(
        bytes32 redeemId,
        address sender,
        uint256 tokenId,
        uint256 burnAmount,
        uint256 burnBalance
    );
    event ApproveNFT(address coll, bool isApproved);

    address public admin; // is a mutil sig address when deploy
    address public parameterControl;

    mapping(address => bool) whiteListNft;
    using SafeMath for uint256;
    using CountersUpgradeable for CountersUpgradeable.Counter;
    CountersUpgradeable.Counter private _redeemNonces;

    struct registering {
        address minter;
        address erc20;
        uint256 amount;
        uint256 balance;
        uint256 price;
    }

    mapping(address => mapping(uint256 => registering)) public mapRegistering;

    struct redeeming {
        uint256 tokenID;
        uint256 burnAmount;
        uint256 burnBalance;
        bool refund;
    }

    mapping(address => mapping(bytes32 => redeeming)) public redeemingHistory;
    address public accountingAddress;
    modifier adminOnly() {
        require(_msgSender() == admin, "ONLY_ADMIN");
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "ONLY_ADMIN");
        _;
    }

    function initialize(address admin_, address parameterControl_)
    public
    initializer
    {
        require(admin_ != address(0x0), "ADDR_INV");
        require(parameterControl_ != address(0x0), "ADDR_INV");

        admin = admin_;
        _setupRole(DEFAULT_ADMIN_ROLE, admin);
        if (admin != _msgSender()) {
            _revokeRole(DEFAULT_ADMIN_ROLE, _msgSender());
        }

        parameterControl = parameterControl_;
    }

    function registerNFT(
        uint256 tokenID,
        address minter,
        uint256 amount,
        address erc20,
        uint256 price,
        uint256 balance
    ) external {
        require(whiteListNft[msg.sender] == true, "NOT_WHITELIST");
        OGNFT autoNFTContract = OGNFT(msg.sender);
        require(
            autoNFTContract.totalSupply(tokenID) > 0,
            "TOKEN_ID_NOT_MINTED"
        );
        require(
            mapRegistering[msg.sender][tokenID].minter == address(0x0),
            "REGISTERED"
        );

        mapRegistering[msg.sender][tokenID].minter = minter;
        mapRegistering[msg.sender][tokenID].amount = amount;
        mapRegistering[msg.sender][tokenID].erc20 = erc20;
        mapRegistering[msg.sender][tokenID].price = price;
        mapRegistering[msg.sender][tokenID].balance = balance;
    }

    function redeem(
        address OGNFTContractAdd,
        uint256 tokenID,
        uint256 burnAmount,
        bool refund
    ) external nonReentrant {
        require(OGNFTContractAdd != address(0x0), "INV_NFT_HOST");
        require(whiteListNft[OGNFTContractAdd] == true, "NOT_WHITELIST");
        require(accountingAddress != address(0x0), "NOT_SET_ACCOUNTING");

        // create offering nonce by counter
        _redeemNonces.increment();
        uint256 newItemId = _redeemNonces.current();
        // init offering id
        bytes32 redeemId = keccak256(
            abi.encodePacked(newItemId, OGNFTContractAdd, tokenID)
        );

        registering memory _registering = mapRegistering[OGNFTContractAdd][
        tokenID
        ];
        OGNFT autoNFTContract = OGNFT(OGNFTContractAdd);

        // require sender owned nft Token id
        require(
            autoNFTContract.balanceOf(msg.sender, tokenID) >= burnAmount,
            "ERC-1155_BALANCE_INVALID"
        );

        // require sender approved for esrow contract
        bool approvalErc1155 = autoNFTContract.isApprovedForAll(
            msg.sender,
            address(this)
        );
        require(approvalErc1155 == true, "ERC-1155_NOT_APPROVED");

        // burn nft token id
        autoNFTContract.burn(msg.sender, tokenID, burnAmount);

        // update registering in escrow
        uint256 burnBalance = burnAmount *
        mapRegistering[OGNFTContractAdd][tokenID].price;
        mapRegistering[OGNFTContractAdd][tokenID].amount -= burnAmount;
        mapRegistering[OGNFTContractAdd][tokenID].balance -= burnBalance;

        // check currency
        bool isERC20 = _registering.erc20 != address(0x0);

        if (msg.sender == _registering.minter) {
            // minter
            if (refund) {
                // transfer refund
                if (isERC20) {
                    ERC20Upgradeable tokenERC20 = ERC20Upgradeable(
                        _registering.erc20
                    );
                    bool success = tokenERC20.transfer(
                        _registering.minter,
                        burnBalance
                    );
                    require(success == true, "TRANSFER_REFUND_FAIL");
                } else {
                    require(address(this).balance > 0, "INVALID_FUND");
                    (bool success,) = _registering.minter.call{
                    value : burnBalance
                    }("");
                    require(success, "TRANSFER_REFUND_FAIL");
                }
                emit MinterRefund(
                    redeemId,
                    _registering.minter,
                    tokenID,
                    _registering.erc20,
                    burnAmount,
                    burnBalance
                );
            } else {
                if (isERC20) {
                    ERC20Upgradeable tokenERC20 = ERC20Upgradeable(
                        _registering.erc20
                    );
                    bool success = tokenERC20.transferFrom(
                        address(this),
                        accountingAddress,
                        burnBalance
                    );
                    require(success == true, "TRANSFER_REFUND_FAIL");
                } else {
                    require(address(this).balance > 0, "INVALID_FUND");
                    (bool success,) = accountingAddress.call{
                    value : burnBalance
                    }("");
                    require(success, "TRANSFER_REFUND_FAIL");
                }

                emit MinterRedeem(
                    redeemId,
                    _registering.minter,
                    tokenID,
                    burnAmount,
                    burnBalance
                );
            }
        } else {
            // others
            if (isERC20) {
                ERC20Upgradeable tokenERC20 = ERC20Upgradeable(
                    _registering.erc20
                );
                bool success = tokenERC20.transferFrom(
                    address(this),
                    accountingAddress,
                    burnBalance
                );
                require(success == true, "TRANSFER_REFUND_FAIL");
            } else {
                require(address(this).balance > 0, "INVALID_FUND");
                (bool success,) = accountingAddress.call{value : burnBalance}(
                    ""
                );
                require(success, "TRANSFER_REFUND_FAIL");
            }

            emit UserRedeem(
                redeemId,
                msg.sender,
                tokenID,
                burnAmount,
                burnBalance
            );
        }
        redeemingHistory[msg.sender][redeemId].tokenID = tokenID;
        redeemingHistory[msg.sender][redeemId].burnAmount = burnAmount;
        redeemingHistory[msg.sender][redeemId].burnBalance = burnBalance;
        redeemingHistory[msg.sender][redeemId].refund = refund;
    }

    // Withdraw
    function withdraw(address _to, address erc_20)
    external
    nonReentrant
    adminOnly
    {
        if (erc_20 == address(0x0)) {
            require(address(this).balance > 0, "NOT_ENOUGH");
            (bool success,) = _to.call{value : address(this).balance}("");
            require(success, "FAIL");
        } else {
            ERC20Upgradeable tokenERC20 = ERC20Upgradeable(erc_20);
            uint256 balance = tokenERC20.balanceOf(address(this));
            require(balance > 0, "NOT_ENOUGH");
            // tranfer erc-20 token
            bool success = tokenERC20.transfer(_to, balance);
            require(success == true, "TRANSFER_FAIL_E");
        }
    }

    function setApproveNft(address OGNFTContract_, bool isApproved)
    external
    adminOnly
    {
        require(OGNFTContract_ != address(0x0), "ADDR_INV");
        whiteListNft[OGNFTContract_] = isApproved;
        emit ApproveNFT(OGNFTContract_, isApproved);
    }

    function setAccountingAddress(address newAccountingAddress)
    external
    adminOnly
    {
        require(newAccountingAddress != address(0x0), "ADDR_INV");
        accountingAddress = newAccountingAddress;
    }
}
