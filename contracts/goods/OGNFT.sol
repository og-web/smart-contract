// SPDX-License-Identifier: MIT
pragma solidity 0.8.12;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";

import "../utils/ERC1155TradableUpgradeable.sol";
import "../governance/ParameterControl.sol";
import "../services/OGEscrow.sol";

/*
 * TODO:
 * [] Use ERC1155 https://docs.openzeppelin.com/contracts/3.x/erc1155
 * []
 *
 */

contract OGNFT is ERC1155TradableUpgradeable {
    event UserCreate(
        address initialOwner,
        uint256 id,
        uint256 initialSupply,
        string uri,
        address creator,
        bytes data
    );

    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;
    uint256 public newItemId;
    address public parameterControlAdd;
    address public erc20TokenFeeUserCreate;
    mapping(uint256 => address) public freezers;
    address public escrowContract;

    function initialize(
        address admin,
        address operator,
        address _parameterControlAdd,
        address _escrowContract,
        string memory name,
        string memory symbol,
        string memory uri
    ) public initializer {
        ERC1155TradableUpgradeable.initialize(
            name,
            symbol,
            uri,
            admin,
            operator
        );
        require(_escrowContract != address(0x0), "INV_ADD");
        escrowContract = _escrowContract;
        require(_parameterControlAdd != address(0x0), "INV_ADD");
        parameterControlAdd = _parameterControlAdd;
        // erc20TokenFeeUserCreate = _erc20;
    }

    function userCreateNFT(
        address _initialOwner,
        uint256 _initialSupply,
        string memory _uri,
        bytes memory _data
    ) external payable returns (uint256) {
        _tokenIds.increment();
        newItemId = _tokenIds.current();

        require(!_exists(newItemId), "A_E");
        require(_initialSupply > 0, "I_I");

        ParameterControl _p = ParameterControl(parameterControlAdd);
        // get fee for mint
        uint256 mintFEE = _p.getUInt256("CREATE_MODEL_FEE");
        erc20TokenFeeUserCreate = _p.getAddress("CREATE_MODEL_FEE_ERC20");
        bool isNativeToken = erc20TokenFeeUserCreate == address(0x0);
        require(escrowContract != address(0x0), "INV_ESCROW");
        OGEscrow escrow = OGEscrow(escrowContract);
        if (mintFEE > 0 && _msgSender() != operator) {
            if (isNativeToken) {
                require(msg.value >= mintFEE * _initialSupply, "I_F");
                (bool success, ) = escrowContract.call{value: msg.value}("");
                require(success, "EscrowContract FAIL");
            } else {
                ERC20Upgradeable tokenERC20 = ERC20Upgradeable(
                    erc20TokenFeeUserCreate
                );
                // tranfer erc-20 token to escrowContract
                bool success = tokenERC20.transferFrom(
                    _msgSender(),
                    address(escrowContract),
                    mintFEE * _initialSupply
                );
                require(success == true, "T_F");
            }
        }

        creators[newItemId] = operator;
        freezers[newItemId] = _msgSender();

        if (bytes(_uri).length > 0) {
            customUri[newItemId] = _uri;
            emit URI(_uri, newItemId);
        }

        _mint(_initialOwner, newItemId, _initialSupply, _data);
        tokenSupply[newItemId] = _initialSupply;

        // register into escrow contract
        escrow.registerNFT(
            newItemId,
            _msgSender(),
            _initialSupply,
            erc20TokenFeeUserCreate,
            mintFEE,
            mintFEE * _initialSupply
        );

        emit UserCreate(
            _initialOwner,
            newItemId,
            _initialSupply,
            _uri,
            _msgSender(),
            _data
        );
        return newItemId;
    }

    function userFreezeCustomURI(uint256 _tokenId, string memory _newURI)
        public
    {
        require(_msgSender() == freezers[newItemId], "ONLY_FREEZER");
        customUri[_tokenId] = _newURI;
        emit URI(_newURI, _tokenId);
    }

    function changeParameterControl(address _new) external adminOnly {
        require(_new != address(0x0), "ADDRESS_INVALID");
        parameterControlAdd = _new;
    }

    function changeUserCreateFee(address _new) external operatorOnly {
        require(_new != address(0x0), "ADDRESS_INVALID");
        erc20TokenFeeUserCreate = _new;
    }

    function setEscrow(address _new) external operatorOnly {
        require(_new != address(0x0), "ADDRESS_INVALID");
        escrowContract = _new;
    }
}
