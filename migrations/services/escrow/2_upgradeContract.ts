import {EscrowContract} from "./escrowContract";

(async () => {
    try {
        if (process.env.NETWORK != "rinkeby") {
            console.log("wrong network");
            return;
        }

        const escrowContract = new EscrowContract(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        const address = await escrowContract.upgradeContract(
            "0xBeB9088a90e4892A04ba35BCC5f1aA440dEF0ace");

        console.log("escrowContract address: " + address)
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();