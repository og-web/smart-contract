import * as dotenv from 'dotenv';

import {EscrowContract} from "./escrowContract";
import {ethers} from "ethers";

(async () => {
    try {
        if (process.env.NETWORK != "rinkeby") {
            console.log("wrong network");
            return;
        }
        const escrowProxyContract = '0xBeB9088a90e4892A04ba35BCC5f1aA440dEF0ace';
        const nftProxyContract = '0xD540F4eA7466084a532325ee5172548397C1024E';
        const nft = new EscrowContract(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
      
        const transactionAddress: any = await nft.setApproveAddress(escrowProxyContract, nftProxyContract, 0);
        console.log("transactionAddress:", transactionAddress);
        //console.log("val", ethers.utils.formatEther(val.value));
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();