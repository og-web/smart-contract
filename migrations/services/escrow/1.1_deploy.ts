import * as dotenv from 'dotenv';

import {EscrowContract} from "./escrowContract";

(async () => {
    try {
        if (process.env.NETWORK != "rinkeby") {
            console.log("wrong network");
            return;
        }
        const nft = new EscrowContract(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        const address = await nft.deployUpgradeable('0x3C67761d096F5F12367E64CecE9E214B9Fe9B731',  '0x55Df4BB8f42d91Db43AeEF5Ba6dc8C011991d349');
        console.log("%s EscrowContract deployed address: %s", process.env.NETWORK, address);
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();