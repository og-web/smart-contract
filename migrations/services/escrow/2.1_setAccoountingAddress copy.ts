import * as dotenv from 'dotenv';

import {EscrowContract} from "./escrowContract";
import {ethers} from "ethers";

(async () => {
    try {
        if (process.env.NETWORK != "rinkeby") {
            console.log("wrong network");
            return;
        }
        const contract = '0xBeB9088a90e4892A04ba35BCC5f1aA440dEF0ace';
        const accountingAddress = '0x9eeDda4E9f2511c7A2d751Bb30073C880Ce5aD71';
        const nft = new EscrowContract(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
      
        const transactionAddress: any = await nft.setAccountingAddress(contract, accountingAddress, 0);
        console.log("transactionAddress:", transactionAddress);
        //console.log("val", ethers.utils.formatEther(val.value));
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();