import * as dotenv from 'dotenv';

import {ParamControl} from "./paramControl";
import {ethers} from "ethers";

(async () => {
    try {
        if (process.env.NETWORK != "rinkeby") {
            console.log("wrong network");
            return;
        }
        const contract = '0x55Df4BB8f42d91Db43AeEF5Ba6dc8C011991d349';
        const key = 'CREATE_MODEL_FEE_ERC20';
        const value = '0xD92E713d051C37EbB2561803a3b5FBAbc4962431'; //usdt https://rinkeby.etherscan.io/token/0x3b00ef435fa4fcff5c209a37d1f3dcff37c705ad
        const nft = new ParamControl(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        let transactionAddress = await nft.setAddress(contract, key, value, 0);
        console.log("%s ParamControl admin address: %s", process.env.NETWORK, transactionAddress);

        const val: any = await nft.getAddress(contract, key);
        console.log("val", val);
        console.log("val", ethers.utils.formatEther(val.value));
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();