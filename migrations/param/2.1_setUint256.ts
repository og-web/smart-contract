import * as dotenv from 'dotenv';

import {ParamControl} from "./paramControl";
import {ethers} from "ethers";

(async () => {
    try {
        if (process.env.NETWORK != "rinkeby") {
            console.log("wrong network");
            return;
        }
        const contract = '0x55Df4BB8f42d91Db43AeEF5Ba6dc8C011991d349';
        const key = 'CREATE_MODEL_FEE';
        const nft = new ParamControl(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        let transactionAddress = await nft.setUInt256(contract, key, 3000000, 0);
        console.log("%s ParamControl admin address: %s", process.env.NETWORK, transactionAddress);

        const val: any = await nft.getUInt256(contract, key);
        console.log("val", val);
        //console.log("val", ethers.utils.formatEther(val.value));
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();