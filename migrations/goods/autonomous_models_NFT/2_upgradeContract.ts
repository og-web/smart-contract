import {OGNFT} from "./OGNFT";

(async () => {
    try {
        if (process.env.NETWORK != "rinkeby") {
            console.log("wrong network");
            return;
        }

        const ogNft = new OGNFT(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        const address = await ogNft.upgradeContract(
            "0xbfe6B966d3073E84cAfe7FD13bAcDaDeF32370eb");

        console.log("escrowContract address: " + address)
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();