// ethereum/scripts/deploy.js
import { createAlchemyWeb3 } from "@alch/alchemy-web3";
import * as path from "path";

const { ethers, upgrades } = require("hardhat");
const hardhatConfig = require("../../../hardhat.config");

class OGNFT {
  network: string;
  senderPublicKey: string;
  senderPrivateKey: string;

  constructor(network: any, senderPrivateKey: any, senderPublicKey: any) {
    this.network = network;
    this.senderPrivateKey = senderPrivateKey;
    this.senderPublicKey = senderPublicKey;
  }

  getContract(contractAddress: any) {
    console.log(
      "Network run",
      this.network,
      hardhatConfig.networks[this.network].url
    );
    if (this.network == "local") {
      console.log("not run local");
      return;
    }
    let API_URL: any;
    API_URL = hardhatConfig.networks[hardhatConfig.defaultNetwork].url;

    // load contract
    let contract = require(path.resolve(
      "./artifacts/contracts/goods/OGNFT.sol/OGNFT.json"
    ));
    const web3 = createAlchemyWeb3(API_URL);
    const nftContract = new web3.eth.Contract(contract.abi, contractAddress);
    return { web3, nftContract };
  }

  async signedAndSendTx(web3: any, tx: any) {
    const signedTx = await web3.eth.accounts.signTransaction(
      tx,
      this.senderPrivateKey
    );
    if (signedTx.rawTransaction != null) {
      let sentTx = await web3.eth.sendSignedTransaction(
        signedTx.rawTransaction,
        function (err: any, hash: any) {
          if (!err) {
            console.log(
              "The hash of your transaction is: ",
              hash,
              "\nCheck Alchemy's Mempool to view the status of your transaction!"
            );
          } else {
            console.log(
              "Something went wrong when submitting your transaction:",
              err
            );
          }
        }
      );
      return sentTx;
    }
    return null;
  }

  async deploy(
    adminAddress: any,
    operatorAddress: any,
    param: string,
    escrow: any,
    name: string,
    symbol: string,
    baseUri: string
  ) {
    if (name.length == 0 || symbol.length == 0) {
      console.log("Name or Symbol is empty");
      return;
    }
    console.log(
      "Network run",
      this.network,
      hardhatConfig.networks[this.network].url
    );
    if (this.network == "local") {
      console.log("not run local");
      return;
    }
    const AvatarNFT = await ethers.getContractFactory("OGNFT");
    const proxy = await upgrades.deployProxy(
      AvatarNFT,
      [
        adminAddress,
        operatorAddress,
        param,
        escrow,
        name,
        symbol,
        baseUri,
      ],
      {
        initializer:
          "initialize(address, address, address, address, string, string, string)",
      }
    );
    await proxy.deployed();

    console.log("OGNFT deployed:", proxy.address);
    return proxy.address;
  }

  async upgradeContract(proxyAddress: any) {
    const contractUpdated = await ethers.getContractFactory("OGNFT");
    upgrades.up;
    console.log("Upgrading OGNFT... by proxy " + proxyAddress);
    const tx = await upgrades.upgradeProxy(proxyAddress, contractUpdated);
    console.log("OGNFT upgraded on tx address " + tx.address);
    return tx;
  }

  async getAdminAddress(contractAddress: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
    };

    const adminAddress: any = await temp?.nftContract.methods.admin().call(tx);
    const operatorAddress: any = await temp?.nftContract.methods
      .operator()
      .call(tx);
    return { adminAddress, operatorAddress };
  }

  async transfer(
    receiver: any,
    contractAddress: any,
    tokenID: number,
    amount: number,
    gas: number
  ) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    let fun = temp?.nftContract.methods.safeTransferFrom(
      this.senderPublicKey,
      receiver,
      tokenID,
      amount,
      "0x"
    );
    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: gas,
      data: fun.encodeABI(),
    };

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }

  async getUri(contractAddress: any, tokenID: number) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
    };

    const uri: any = await temp?.nftContract.methods.uri(tokenID).call(tx);
    return uri;
  }

  async setCustomTokenUri(
    contractAddress: any,
    tokenID: number,
    newTokenURI: string,
    gas: number
  ) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    const fun = temp?.nftContract.methods.setCustomURI(tokenID, newTokenURI);
    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: gas,
      data: fun.encodeABI(),
    };

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }

  async setTokenUri(contractAddress: any, newTokenURI: string, gas: number) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    const fun = temp?.nftContract.methods.setURI(newTokenURI);
    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: gas,
      data: fun.encodeABI(),
    };

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }

  async userCreateNFT(
    initOwnerAddress: any,
    contractAddress: any,
    initSupply: number,
    tokenURI: string = "",
    gas: number
  ) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    const fun = temp?.nftContract.methods.userCreateNFT(
      initOwnerAddress,
      initSupply,
      tokenURI,
      ""
    );

    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: gas,
      data: fun.encodeABI(),
    };
    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }
    return await this.signedAndSendTx(temp?.web3, tx);
  }

  async newItemId(contractAddress: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
    };

    const newItemId: any = await temp?.nftContract.methods.newItemId().call(tx);
    return newItemId;
  }

  async setCreator(
    contractAddress: any,
    creatorAddress: any,
    ids: number[],
    gas: number
  ) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    const fun = temp?.nftContract.methods.setCreator(creatorAddress, ids);
    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: gas,
      data: fun.encodeABI(),
    };

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }

  async getProxyRegisterAddress(contractAddress: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
    };

    const proxyRegistryAddress: any = await temp?.nftContract.methods
      .proxyRegistryAddress()
      .call(tx);
    return proxyRegistryAddress;
  }

  async isApprovedForAll(contractAddress: any, owner: any, operator: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 500000,
      // data: null,
    };
    try {
      const result: any = await temp?.nftContract.methods
        .isApprovedForAll(owner, operator)
        .call(tx);
      console.log(result.hash);
      return result;
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  async setProxyRegisterAddress(
    contractAddress: any,
    proxyAddress: any,
    gas: number
  ) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: gas,
      data: temp?.nftContract.methods
        .setProxyRegistryAddress(proxyAddress)
        .encodeABI(),
    };

    return await this.signedAndSendTx(temp?.web3, tx);
  }

  async setApprovalForAll(contractAddress: any, operator: any, gas: number) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: gas,
      data: temp?.nftContract.methods
        .setApprovalForAll(operator, true)
        .encodeABI(),
    };

    return await this.signedAndSendTx(temp?.web3, tx);
  }

  async withdraw(to: any, erc20: any, contractAddress: any, gas: number) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    const fun = temp?.nftContract.methods.withdraw(to, erc20);
    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: gas,
      data: fun.encodeABI(),
    };

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }

  async getTotalSupply(contractAddress: any, tokenId: number) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
    };

    const tokenSupply: any = await temp?.nftContract.methods
      .tokenSupply(tokenId)
      .call(tx);
    return tokenSupply;
  }

  async changeParameterControl(contractAddress: any, _new: any, gas: number) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(
      this.senderPublicKey,
      "latest"
    ); //get latest nonce

    // const eth = ethers.utils.parseEther(price);
    const fun = temp?.nftContract.methods.changeParameterControl(_new);
    //the transaction
    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: gas,
      data: fun.encodeABI(),
    };

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }
}

export { OGNFT };
