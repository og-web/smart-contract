import { OGNFT } from "./OGNFT";
import { ethers } from "ethers";

(async () => {
  try {
    if (process.env.NETWORK != "mumbai") {
      console.log("wrong network");
      return;
    }

    let initOwnerAddress = "0xF61234046A18b07Bf1486823369B22eFd2C4507F";
    let initSupply = 1;
    let max: any = 10000;
    let nftContract = "0xed9D344f8b1402AD2EbEa6393563e3fEa9b1C6FB";
    const erc20 = "0x0000000000000000000000000000000000000000";
    const price = ethers.utils.parseEther("1000000");

    const nft = new OGNFT(
      process.env.NETWORK,
      process.env.PRIVATE_KEY,
      process.env.PUBLIC_KEY
    );
    let newItemId = await nft.newItemId(nftContract);
    console.log({ newItemId });
    while (newItemId < 1) {
      const tx = await nft.userCreateNFT(
        initOwnerAddress,
        nftContract,
        initSupply,
        "",
        0
      );
      console.log({ tx });
      newItemId = await nft.newItemId(nftContract);
      console.log({ newItemId });
    }
  } catch (e) {
    // Deal with the fact the chain failed
    console.log(e);
  }
})();
