import { OGNFT } from "./OGNFT";

const { ethers } = require("hardhat");
const { getContractAddress } = require("@ethersproject/address");

(async () => {
  try {
    if (process.env.NETWORK != "rinkeby") {
      console.log("wrong network");
      return;
    }

    const nft = new OGNFT(
      process.env.NETWORK,
      process.env.PRIVATE_KEY,
      process.env.PUBLIC_KEY
    );

    const [owner] = await ethers.getSigners();
    const transactionCount = await owner.getTransactionCount();

    const futureAddress = getContractAddress({
      from: owner.address,
      nonce: transactionCount,
    });
    console.log({ futureAddress });
    // return;
    const chainID = "1";
    const baseUrl =
      "https://linkToAPI/metadata/" + chainID + "/" + futureAddress + "/";
    const address = await nft.deploy(
      "0x3C67761d096F5F12367E64CecE9E214B9Fe9B731",
      "0x3C67761d096F5F12367E64CecE9E214B9Fe9B731",
      "0x55Df4BB8f42d91Db43AeEF5Ba6dc8C011991d349",
      "0xBeB9088a90e4892A04ba35BCC5f1aA440dEF0ace",
      "test-deploy-nft",
      "symbol",
      baseUrl
    );
    console.log("Model NFT deployed address: ", address);
  } catch (e) {
    // Deal with the fact the chain failed
    console.log(e);
  }
})();
