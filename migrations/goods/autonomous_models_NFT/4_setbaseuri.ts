import { OGNFT } from "./OGNFT";

(async () => {
  try {
    if (process.env.NETWORK != "rinkeby") {
      console.log("wrong network");
      return;
    }

    const contractAdd = "0xD540F4eA7466084a532325ee5172548397C1024E";
   // const tokenID = 0;
    const nft = new OGNFT(
      process.env.NETWORK,
      process.env.PRIVATE_KEY,
      process.env.PUBLIC_KEY
    );
   // const currentBaseUri = await nft.getUri(contractAdd, tokenID);
  //  console.log({ uri: currentBaseUri });
    const tx = await nft.setTokenUri(contractAdd, "https://og.dev.autonomousdev.xyz/api/v1/metadata/4/0xD540F4eA7466084a532325ee5172548397C1024E/", 0);
    console.log({ tx });
  } catch (e) {
    // Deal with the fact the chain failed
    console.log(e);
  }
})();
